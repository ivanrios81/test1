$(document).ready(function(){
  //$(".rslides").responsiveSlides();

   $("#slider4").responsiveSlides({
        auto: true,
        pager: false,
        nav: true,
        speed: 500,
        namespace: "callbacks",
        before: function () {
          $('.events').append("<li>before event fired.</li>");
        },
        after: function () {
          $('.events').append("<li>after event fired.</li>");
        }
      });

          $('section[data-type="parallax_section"]').each(function(){
              var $bgobj = $(this); // Variable para asignacion de objeto
              $(window).scroll(function() {
                $window = $(window);
                  var yPos = -($window.scrollTop() / $bgobj.data('speed'));
                  // cordinadas del background
                  var coords = '50%'+ yPos + 'px';
                  // moviendo el background
                  $bgobj.css({ backgroundPosition: coords });
              });
          });
      

    $(".menu-btn-movil").click(function(){
        $(".menu-content-movil").slideToggle(1000);
       });
   
   
});

